#!/bin/bash
# basics

# monitor setup
xrandr --output DVI-D-0 --off --output HDMI-0 --off --output DP-1 --off --output DP-0 --mode 3440x1440 --pos 0x0 --rotate normal --rate 144.00
# xrandr --output DVI-D-0 --off --output HDMI-0 --mode 1920x1080 --pos 0x0 --rotate normal --output DP-0 --mode 3440x1440 --pos 1920x0 --rotate normal --rate 144.00 --output DP-1 --off &
picom --config $HOME/.config/picom/picom.conf &     # compositor
dunst &                                             # notifications
# amixer -D pulse sset Master 100% unmute &           # volume 100%
feh --bg-fill --randomize ~/Pictures/tapet/* &      # wallpaper
# redshift-gtk &                                      # blue light filter
redshift-gtk -l 41.74759:-74.08681 &
nm-applet &                                         # network manager
unclutter -idle 5 &                                 # hide mouse
# unclutter --timeout 5 &
udiskie &                                           # usb drive daemon
wal -R &                                            # terminal colors
sxhkd &                                             # extra keybinds
# $HOME/.config/polybar/launch.sh &

# apps
firefox &
# discord &
pulseeffects &
