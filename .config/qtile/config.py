# [silly ascii art that makes me look cooler than i really am]

# import socket
import os
import subprocess
from typing import List  # noqa: F401

from libqtile import qtile
from libqtile import bar, hook, layout, widget
from libqtile.command import lazy
from libqtile.config import Click, Drag, Group, Key, Screen, ScratchPad, DropDown

cursor_warp = True
# cursor_warp = False

wmname = 'qtile'
home = os.path.expanduser('~')
with open(home + '/.cache/wal/colors') as f:
    colors = f.read().splitlines()

kc = []
with open(home + '/.config/qtile/keys') as f:
    for line in f:
        kc.append([str(n) for n in line.split()])

mod = 'mod4'
alt = 'mod1'
ctl = 'control'
sft = 'shift'
my_term = 'alacritty'
my_mono = 'Source Code Pro Medium'
my_font = 'Quicksand Medium'
my_rofi = 'rofi\
    -show run\
    -location 0\
    -width 32\
    -lines 16\
    -columns 1\
    -line-margin 1\
    -line-padding 1\
    -font \'Source Code Pro Medium 24\'\
    -hide-scrollbar\
'
# these 2 don't work
init_qwerty = 'ln -sf ~/.config/qtile/qwertykeys ~/.config/qtile/keys'
init_colemak = 'ln -sf ~/.config/qtile/colemakdhmkeys ~/.config/qtile/keys'

keys = [
    # The essentials

    Key([mod], 'Return',
        lazy.spawn(my_term)),

    Key([mod], kc[1][4],
        lazy.spawn(my_rofi)),

    Key([mod, sft], kc[0][9],
        lazy.spawncmd()),

    Key([mod, sft], kc[1][4],
        lazy.spawncmd()),

    Key([mod, ctl], kc[1][5],
        lazy.next_layout()),

    Key([mod], kc[0][0],
        lazy.window.kill()),

    Key([mod, ctl], kc[1][4],
        lazy.restart()),

    Key([mod, ctl, sft], kc[0][0],
        lazy.shutdown()),

    # Key([mod], kc[0][6],
    #     lazy.spawn('betterscreenlock -l')),  # TODO doesn't work

    Key([mod], kc[0][3],
        lazy.hide_show_bar('all')),

    # Docking/Layout changing

    Key([mod], 'BackSpace',
        lazy.spawn('')),

    #  Key([mod], 'F1',  # nonfunctional
    #  lazy.spawn(init_qwerty)),
    #
    #  Key([mod], 'F2',  # nonfunctional
    #  lazy.spawn(init_colemak)),

    # Key([mod, sft], kc[1][4],
    #     lazy.spawn('autorandr -c')),

    # Window controls

    Key([mod], kc[1][6],
        lazy.layout.left()),

    Key([mod], kc[1][7],
        lazy.layout.down()),

    Key([mod], kc[1][8],
        lazy.layout.up()),

    Key([mod], kc[1][9],
        lazy.layout.right()),

    Key([mod, sft], kc[1][6],
        lazy.layout.shuffle_left()),

    Key([mod, sft], kc[1][7],
        lazy.layout.shuffle_down()),

    Key([mod, sft], kc[1][8],
        lazy.layout.shuffle_up()),

    Key([mod, sft], kc[1][9],
        lazy.layout.shuffle_right()),

    Key([mod, sft], kc[1][5],
        lazy.layout.flip()),

    Key([mod], kc[2][6],
        lazy.layout.grow_left()),
    #  lazy.layout.shrink()),

    Key([mod], kc[2][7],
        lazy.layout.grow_down()),
    #  lazy.layout.shrink()),

    Key([mod], kc[2][8],
        lazy.layout.grow_up()),
    #  lazy.layout.grow()),

    Key([mod], kc[2][9],
        lazy.layout.grow_right()),
    #  lazy.layout.grow()),

    Key([mod, sft], kc[2][6],
        lazy.layout.flip_left()),

    Key([mod, sft], kc[2][7],
        lazy.layout.flip_down()),

    Key([mod, sft], kc[2][8],
        lazy.layout.flip_up()),

    Key([mod, sft], kc[2][9],
        lazy.layout.flip_right()),

    Key([mod], 'space',
        lazy.next_screen()),

    Key([mod, ctl, sft], kc[2][5],
        lazy.window.toggle_floating()),

    # Essential applications lauched with SUPER + [top row key]

    Key([mod], kc[0][5],
        lazy.spawn('firefox')),

    Key([mod], kc[0][6],
        lazy.spawn('discord')),

    Key([mod], kc[0][7],
        lazy.spawn('pcmanfm')),

    Key([mod], kc[0][8],
        lazy.spawn(my_term + ' -e fff')),

    Key([mod], kc[0][9],
        #  lazy.spawn(my_term + ' -e nvim')),
        lazy.spawn('gvim')),

    # System Keys

    Key([], 'XF86AudioMute',
        lazy.spawn('amixer -q set Master toggle')),

    Key([], 'XF86AudioLowerVolume',
        lazy.spawn('amixer -c 0 sset Master 5- unmute')),

    Key([], 'XF86AudioRaiseVolume',
        lazy.spawn('amixer -c 0 sset Master 5+ unmute')),

    Key([], 'XF86MonBrightnessDown',
        lazy.spawn('xbacklight -dec 5')),

    Key([], 'XF86MonBrightnessUp',
        lazy.spawn('xbacklight -inc 5')),

    Key([], 'Print',
        lazy.spawn('flameshot gui')),

    Key([mod], 'F1',
        lazy.group['scratchpad'].dropdown_toggle('term')),
]

# Group names/keys
groups = [
    Group(kc[1][0], layout='Bsp'),
    Group(kc[1][1], layout='Bsp'),
    Group(kc[1][2], layout='Bsp'),
    Group(kc[1][3], layout='Bsp'),
    Group(kc[2][0], layout='Bsp'),
    Group(kc[2][1], layout='Bsp'),
    Group(kc[2][2], layout='Bsp'),
    Group(kc[2][3], layout='Bsp'),
]

# groups = [Group(name, **kwargs) for name, kwargs in group_names]

groups.append(
    ScratchPad('scratchpad', [
        # define a drop down terminal.
        # it is placed in the upper third of screen by default.
        DropDown('term', my_term, height=0.75),
    ])
)

# for i, (name, kwargs) in enumerate(groups, 1):
#     keys.extend([
#         Key([mod], name,
#             lazy.group[name].toscreen(toggle=False)),
#         Key([mod, sft], name,
#             lazy.window.togroup(name)),
#     ])
for i in ['a', 'r', 's', 't', 'z', 'x', 'c', 'd']:
    keys.extend([
        Key([mod], i,
            lazy.group[i].toscreen(toggle=False)),
        Key([mod, sft], i,
            lazy.window.togroup(i)),
    ])

# Layouts
layout_theme = {
    'border_width': 2,
    'margin': 4,
    'border_focus': colors[7],
    'border_normal': colors[0],
}

layouts = [
    layout.Bsp(**layout_theme, fair=False, grow_amount=2),
    layout.Columns(**layout_theme, num_columns=3, insert_position=1),
    #  layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    # layout.Floating(**layout_theme),
]


# Widgets

def get_memory():
    return (subprocess.check_output(['/home/qouesm/.config/qtile/memory.sh']).decode('utf-8').strip())


widget_defaults = dict(
    font=my_mono,
    fontsize=12,
    padding=10,
    update_interval=5,
    background=colors[0],
    foreground=colors[0],
)


def init_widgets_list():
    widgets_list = [
        widget.CurrentLayoutIcon(
            scale=0.5,
            padding=5,
        ),
        # widget.CurrentLayout(
        #     background=colors[1],
        # ),
        # widget.CurrentScreen(
        #     active_color = colors[1],
        #     active_text = '',
        #     inactive_color = colors[0],
        #     inactive_text = '',
        # ),
        widget.GroupBox(
            padding=5,
            margin_x=10,
            active=colors[7],
            inactive=colors[0],
            disable_drag=True,
            rounded=False,
            highlight_method='block',

            # active screen; visible group
            this_current_screen_border=colors[1],
            # non-active screen; visible group
            this_screen_border=colors[4],
            # active screen; non-visible group
            other_current_screen_border=colors[8],
            # non-active screen; non-visible group
            other_screen_border=colors[8],
        ),
        widget.Prompt(
            background=colors[1],
            foreground=colors[0],
        ),
        widget.WindowName(
            foreground=colors[7],
            for_current_screen=True,
        ),
        widget.Spacer(length=5, background=colors[1], foreground=colors[1]),
        widget.TextBox(
            '',
            background=colors[1],
            fontsize=30,
            padding=0,
        ),
        widget.CPU(
            format='{load_percent}%',
            background=colors[1],
        ),
        # widget.Spacer(length=10),
        # widget.CPUGraph(
        #     background=colors[1],
        #     graph_color=colors[0],
        #     fill_color=colors[0],
        #     border_color=colors[1],
        #     samples=500,
        # ),
        widget.Spacer(length=10),
        widget.ThermalSensor(
            background=colors[1],
            foreground=colors[0],
        ),
        widget.Spacer(length=10),
        # widget.Memory(
        #     format='{MemUsed} MB',
        #     background=colors[3],
        # ),
        widget.Spacer(length=5, background=colors[5], foreground=colors[5]),
        widget.TextBox(
            '',
            background=colors[5],
            fontsize=30,
            padding=0,
        ),
        widget.GenPollText(  # MEMORY
            func=get_memory,
            background=colors[5],
        ),
        # widget.Spacer(length=10),
        # widget.MemoryGraph(
        #     background=colors[5],
        #     graph_color=colors[0],
        #     fill_color=colors[0],
        #     border_color=colors[5],
        # ),
        # widget.Spacer(length=10),
        # widget.TextBox('  '),
        # widget.Volume(
        #     update_interval = 0.2,
        #     volume_up_command='amixer -q sset Master 5%+',
        #     volume_down_command='amixer -q sset Master 5%-',
        # ),
        widget.Spacer(length=10),
        widget.Clock(
            format='%a %d',
            background=colors[3],
        ),
        widget.Spacer(length=10),
        widget.Clock(
            format='%H:%M',
            background=colors[3],
        ),
        widget.Spacer(length=5),
    ]
    return widgets_list


# Screens

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    widgets_screen1.extend([
        widget.Systray(),
        widget.Spacer(length=15),
    ])
    return widgets_screen1


def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2


def init_screens_default():
    return [
        Screen(
            top=bar.Bar(
                widgets=init_widgets_screen1(),
                opacity=0.90,
                size=24,
            ),
        ),
    ]


def init_screens_dual():
    return [
        Screen(
            top=bar.Bar(
                widgets=init_widgets_screen1(),
                opacity=0.90,
                size=24,
            ),
        ),
        Screen(
            top=bar.Bar(
                widgets=init_widgets_screen2(),
                opacity=0.90,
                size=24,
            ),
        ),
    ]


if __name__ in ['config', '__main__']:
    screens = init_screens_default()
    # screens = init_screens_dual()

# Drag floating windows
mouse = [
    Drag([mod], 'Button1',
         lazy.window.set_position_floating(),
         start=lazy.window.get_position()),

    Drag([mod], 'Button2',
         lazy.window.set_size_floating(),
         start=lazy.window.get_size()),

    Click([mod], 'Button3',
          lazy.window.bring_to_front()),
]

auto_fullscreen = True
bring_front_click = True
focus_on_window_activation = 'smart'
follow_mouse_focus = True

# Define floating rules
# `xprop` to get wmclass
# floating_layout = layout.Floating(
#     float_rules=[
#         {'wmclass': 'confirm'},
#         {'wmclass': 'dialog'},
#         {'wmclass': 'download'},
#         {'wmclass': 'error'},
#         {'wmclass': 'file_progress'},
#         {'wmclass': 'notification'},
#         {'wmclass': 'splash'},
#         {'wmclass': 'toolbar'},
#         {'wmclass': 'confirmreset'},  # gitk
#         {'wmclass': 'makebranch'},  # gitk
#         {'wmclass': 'maketag'},  # gitk
#         {'wname': 'branchdialog'},  # gitk
#         {'wname': 'pinentry'},  # GPG key password entry
#         {'wmclass': 'ssh-askpass'},  # ssh-askpass
#         {'wmclass': 'steam'},
#     ]
# )


# Hooks

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.Popen([home + '/.config/qtile/autostart.sh'])


@hook.subscribe.startup
def restart():
    home = os.path.expanduser('~')
    subprocess.Popen([home + '/.config/qtile/restart.sh'])
