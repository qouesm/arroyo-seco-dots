local M = {}

M.ui = {
  theme_toggle = { "onedark", "one_light" },
  theme = "gruvbox",
}

vim.opt.relativenumber = true

M.plugins = require "custom.plugins"

M.plugins = {
  ["jose-elias-alvarez/null-ls.nvim"] = {
    after = "nvim-lspconfig",
    config = function()
     require "custom.plugins.null-ls"
    end,
  },

  ["neovim/nvim-lspconfig"] = {
    config = function()
      require "plugins.configs.lspconfig"
      require "custom.plugins.lspconfig"
    end,
  },

  ["williamboman/mason.nvim"] = {
    override_options = {
      ensure_installed = {
        -- lua stuff
        "lua-language-server",
        "stylua",

        -- shell
        "shfmt",
        "shellcheck",

        -- python
        "autopep8",
        "debugpy",
        "flake8",
        "isort",
        "jedi-language-server",
        "pylint",

        -- web
        "css-lsp",
        "html-lsp",
        "typescript-language-server",
        "deno",
        "json-lsp",
        "rome",

        -- java
        "jdtls"
      },
    },
  },

  ["lervag/vimtex"] = {},
}

vim.g.vimtex_compiler_progname = "nvr"
vim.g.vimtex_view_method = "mupdf"
vim.g.vimtex_compiler_latexmk = {
  ["options"] = {
    "-output-directory=build"
  },
}

-- check core.mappings for table structure
M.mappings = require "custom.mappings"

return M
