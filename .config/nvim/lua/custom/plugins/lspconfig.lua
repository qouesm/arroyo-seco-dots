local on_attach = require("plugins.configs.lspconfig").on_attach
local capabilities = require("plugins.configs.lspconfig").capabilities

local lspconfig = require "lspconfig"

local servers = { "html", "cssls", "tsserver", "clangd", "jedi_language_server" }

for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,
  }
end

require'lspconfig'.jedi_language_server.setup{}

-- local present, null_ls = pcall(require, "null-ls")
--
-- if not present then
--   return
-- end
--
-- local b = null_ls.builtins
--
-- local sources = {
--   -- python
--   b.diagnostics.flake8,
--   b.diagnostics.pylint,
--   b.formatting.autopep8,
--   b.formatting.isort,
--
--   -- tbd
--   b.formatting.stylua,
--   b.formatting.shfmt,
-- }
--
-- null_ls.setup {
--   sources = sources,
-- }
--
