local present, null_ls = pcall(require, "null-ls")

if not present then
  return
end

local b = null_ls.builtins
local sources = {

    -- python
    null_ls.builtins.diagnostics.flake8,
    null_ls.builtins.formatting.autopep8,
    null_ls.builtins.formatting.isort,

    -- Lua
    null_ls.builtins.formatting.stylua,

    -- Shell
    null_ls.builtins.formatting.shfmt,
    null_ls.builtins.diagnostics.shellcheck.with { diagnostics_format = "#{m} [#{c}]" },

  -- -- webdev stuff
  -- b.formatting.deno_fmt,
  -- b.formatting.prettier.with { filetypes = { "html", "markdown", "css" } },

  -- cpp
  b.formatting.clang_format,
  b.formatting.rustfmt,
}

null_ls.setup {
  debug = true,
  sources = sources,
}


